import importlib
import os
import renpy.parser as parser
import renpy.ast as ast
from renpy import display 
from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

def compile_to(code, screen):
    compiled = parser.parse("FNDummy", code)
    for node in compiled:
        if isinstance(node, ast.Init):
            for child in node.block[0].screen.children:
                modast.get_slscreen(screen).children.append(child)

@loadable_mod
class AWSWMod(Mod): 
    def mod_info(self):
        return ("AwSW Randomizer", "0.1.1", "Jakzie")
    
    def mod_load(self):
        mod_path = modinfo.get_mod_path("AwSW Randomizer")
        randomizers = []
        
        for python_file in os.listdir(mod_path):
            if os.path.isdir(os.path.join(mod_path, python_file)) or not python_file.startswith("rng_") or not python_file.endswith(".py"):
                continue
            randomizers.append(importlib.import_module(self.__module__ + "." + python_file[:-3]))
        
        tocompile = """
        screen dummy:
            imagebutton auto "ui/random_button_%s.png" action [Show('jz_randomizer_menu', transition=dissolve), Play("audio", "se/sounds/open.ogg")] hovered Play("audio", "se/sounds/select.ogg") xalign 0.97 yalign 0.955
            """

        compile_to(tocompile, "main_menu")
        
        rng_menu_base = """
        screen dummy:
            frame:
                add "ui/randomizer_bg.png" xalign 0.5 yalign 0.5 at alpha_dissolve
                add "image/ui/ingame_menu_bg_light.png" at ingame_menu_light
            
            imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("jz_randomizer_menu", transition=dissolve), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" xalign 0.945 yalign 0.035 at nav_button
                
            frame id "jz_randomizer_menu":
                
                default tt = Tooltip(".")
                
                hbox: 
                    vbox ypos 0.3:
                        vbox yalign 0.5:{}
                
                    vbox ypos 0.3:
                        text tt.value
        """
        
        rng_button_base = """
        
                            textbutton "{}":
                                action [Show("jz_randomizer_menu_exec", transition=Dissolve(1.0)), SetVariable("jz_randomizer_rng_id", {}), Function(jz_randomizer_run), Play("audio", "se/sounds/open.ogg"), Hide("jz_randomizer_menu")]
                                hovered [Play("audio", "se/sounds/select.ogg"), tt.Action("{}")]
                                style "menubutton2"
        """
        
        colors = {"low": "00ff00", "medium": "ffdd00", "high": "ff8a00", "extreme": "ff0000"}
        
        rng_strings = {"low": [], "medium": [], "high": [], "extreme": []}
        
        for i, randomizer in enumerate(randomizers):
            button_text, _, tooltip_text, chaos_index = randomizer.rng_config()
            rng_strings[chaos_index].append(rng_button_base.format("{color=#" + colors[chaos_index] + "}" + button_text + "{/color}", i, tooltip_text))
        
        rng_strings_all = rng_strings["low"] + rng_strings["medium"] + rng_strings["high"] + rng_strings["extreme"]
        rng_finished_menu = rng_menu_base.format("\n".join(rng_strings_all))
        compile_to(rng_finished_menu, "jz_randomizer_menu")
        
        modast.set_renpy_global("jz_randomizer_rng_id", -1)
        
        def jz_randomizer_run():
            randomizer = randomizers[modast.get_renpy_global("jz_randomizer_rng_id")]
            randomizer.rng_execute()
            randomizer_msg = randomizer.rng_config()[1]
            modast.set_renpy_global("jz_randomizer_rng_label", randomizer_msg)
            print("[AwSW Randomizer]: " + randomizer_msg)
        
        modast.set_renpy_global("jz_randomizer_run", jz_randomizer_run)
        
        modast.call_hook(modast.find_label("nameentry"), modast.find_label("jz_randomizer_vars"))
        
    
    def mod_complete(self):
        pass
