import renpy
from renpy import ast
import random

def rng_config():
    return (
        "Randomize character lines", 
        "Lines of all characters have been randomized!",
        "For each character it shuffles their lines.",
        "medium"
    )

def rng_execute():
    character_lines = {}
    language = renpy.game.preferences.language
    
    if language is None:
        language_says = set((tl.block[0] for tl in renpy.game.script.translator.language_translates.values()))
    else:
        language_says = set((tl[1].block[0] for tl in renpy.game.script.translator.language_translates.items() if tl[0][1] == language))
    
    def ignore_node(node):
        if node.what is None or node.what.strip() == "":
            return True
        if language is None and node in language_says:
            return True
        if language is not None and node not in language_says:
            return True
        return False
        
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Say):
            if ignore_node(node):
                continue
            if node.who not in character_lines:
                character_lines[node.who] = [node.what]
            else:
                character_lines[node.who].append(node.what)
    
    for ch, lines in character_lines.items():
        random.shuffle(lines)
    
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Say):
            if ignore_node(node):
                continue
            node.what = character_lines[node.who].pop()
