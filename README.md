# AWSW-Randomizer

AwSW Randomizer is a mod for the game Angels with Scaly Wings which adds a new menu allowing you to randomize various aspects of the game. Chaos guaranteed.

## Contributing

If you want to add your own randomizer, create a file `rng_yourname_randomizername.py`. In the file there have to be these two functions:

```python
def rng_config():
    return (
        "text of the button", 
        "text displayed after randomizer finishes",
        "description of the randomizer",
        "chaos index"
    )

def rng_execute():
    # Put the randomization code here
```

The *chaos index* describes how much the randomizer can mess with the gameplay. It can have one of the following values:

- `"low"` *(green)*: The game is still playable without any issues and the changes are mostly esthetic.
- `"medium"` *(yellow)*: Gameplay is effected, but in a non-breaking way, so the player can still finish the game.
- `"high"` *(orange)*: Significant changes to gameplay. It may not be possible to finish the game without being lucky.
- `"extreme"` *(red)*: The game becomes a total chaos and it doesn't even feel like AwSW anymore.

