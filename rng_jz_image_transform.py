import random
import renpy
from renpy.display import im

def rng_config():
    return (
        'Randomize image transformations', 
        'Image transformations were randomized!',
        'Randomizes the rotation, scale and color of all named images.',
        "low"
    )

def rng_execute():
    for k, v in renpy.display.image.images.items():
        try:
            image = im.image(v)
        except Exception:
            continue
        
        matrix = (
            random.uniform(0.5, 1.0), random.uniform(0.0, 0.5), random.uniform(0.0, 0.5), 0, 0,
            random.uniform(0.0, 0.5), random.uniform(0.5, 1.0), random.uniform(0.0, 0.5), 0, 0,
            random.uniform(0.0, 0.5), random.uniform(0.0, 0.5), random.uniform(0.5, 1.0), 0, 0,
            0, 0, 0, 1, 0,
        )
        
        image = im.MatrixColor(image, matrix)
        image = im.Rotozoom(image, random.randrange(360), 1.0)
        image = im.FactorScale(image, random.uniform(0.5, 2.0), random.uniform(0.5, 2.0))
        flip = (bool(random.getrandbits(1)), bool(random.getrandbits(1)))
        if flip[0] or flip[1]:
            image = im.Flip(image, horizontal=flip[0], vertical=flip[1])
        
        renpy.display.image.images[k] = image
        
