import renpy
from renpy import ast
import random

def rng_config():
    return (
        "Randomize character names and menu choice text", 
        "The character names and menu choice text have been randomized!",
        "Shuffles character names and text of menu choices.\\nThe position of choices stays the same.",
        "medium"
    )

def rng_execute():
    blacklist = ["English {image=lang-en.png}", "English {image=lang-en2.png}", "{color=#0000ff}B{/color}lue", "{color=#00ffff}C{/color}yan", "{color=#ffd700}G{/color}old", "{color=#808000}O{/color}live", "No"]
        
    strings = []
        
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Menu):
            if node.items[0][0] in blacklist:
                continue
            for item in node.items:
                strings.append(item[0])
    
    for gl in renpy.python.store_dicts["store"].values():
        if isinstance(gl, renpy.character.ADVCharacter):
            if gl.name is not None:
                strings.append(gl.name)
    
    random.shuffle(strings)
    
    for node in renpy.game.script.all_stmts:
        if isinstance(node, ast.Menu):
            if node.items[0][0] in blacklist:
                continue
            for i, item in enumerate(node.items):
                node.items[i] = (strings.pop(), item[1], item[2])
    
    for gl in renpy.python.store_dicts["store"].values():
        if isinstance(gl, renpy.character.ADVCharacter):
            if gl.name is not None:
                gl.name = strings.pop()
