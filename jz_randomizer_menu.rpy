screen jz_randomizer_menu tag smallscreen:
    modal True

screen jz_randomizer_menu_exec:
    modal True
    
    window id "jz_randomizer_menu_exec" at popup2:
        style "alertwindow"
                
        vbox xalign 0.5 yalign 0.5:
            label "[jz_randomizer_rng_label]":
                style "yesno_prompt"
        
        imagebutton:
            idle "image/ui/close_idle.png"
            hover "image/ui/close_hover.png"
            action [Show("jz_randomizer_menu"), Hide("jz_randomizer_menu_exec"), Hide("preferencesbg", transition=dissolve), ToggleVariable('navmenuopen', False), Play("audio", "se/sounds/close.ogg")]
            hovered Play("audio", "se/sounds/select.ogg")
            style "smallwindowclose"
            at nav_button
